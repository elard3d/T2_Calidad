﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorio
{
    public interface IHomeRepository
    {
        List <Libro> GetLibros();
    }

    public class HomeRepository : IHomeRepository
    {

        private IAppBibliotecaContext context;

        public HomeRepository(IAppBibliotecaContext context)
        {
            this.context = context;
        }

        public List<Libro> GetLibros()
        {
            return context.Libros.Include("Libros Autores").ToList();
        }
    }
}
