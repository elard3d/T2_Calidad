﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using CalidadT2.Repositorio;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private IHomeRepository repository;
        
        public HomeController(IHomeRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public IActionResult Index()
        {            
            var model = repository.GetLibros();
            return View("Index", model);
        }

        private string GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            return claim.Value;
        }
    }
}
