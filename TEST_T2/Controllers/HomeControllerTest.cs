﻿using Moq;
using NUnit.Framework;
using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Text;
using CalidadT2.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using CalidadT2.Repositorio;

namespace TEST_T2.Controllers
{
    [TestFixture]
    class HomeControllerTest
    {

        [Test]
        public void HomeTest1()
        {

            var repository = new Mock<IHomeRepository>();
            repository.Setup(o => o.GetLibros()).Returns(new List<Libro>());
            var controller = new HomeController(repository.Object);
            var view = controller.Index() as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }
    }
}
